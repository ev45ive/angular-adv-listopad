
npm install --global nx create-nx-workspace @angular/cli

https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode

create-nx-workspace sgs
? What to create in the new workspace angular           [a workspace with a single Angular application]
? Application name                    szkolenie
? Default stylesheet format           SASS(.scss)  [ http://sass-lang.com   ]
? Default linter                      TSLint [ Used by Angular CLI ]
? Use Nx Cloud? (It's free and doesn't require registration.) No

nx new sgs  --preset="angular" --appName="szkolenie" --style="scss" --linter="tslint" --interactive=false --collection=@nrwl/workspace


ng generate @schematics/angular:module --name=playlists --project=szkolenie --module=app --route=playlists --routing

// app/playlists/playlists.component.scss
:host{
  display: block;
}
// angular.json:
     "schematics": {
        "@schematics/angular:component": {
          "style": "scss",
          "displayBlock": true
        }

# Material
cd apps/szkolenie
ng add @angular/material
? Choose a prebuilt theme name, or "custom" for a custom theme: (Use arrow keys)
> Indigo/Pink        [ Preview: https://material.angular.io?theme=indigo-pink ]
  Deep Purple/Amber  [ Preview: https://material.angular.io?theme=deeppurple-amber ]
  Pink/Blue Grey     [ Preview: https://material.angular.io?theme=pink-bluegrey ]
  Purple/Green       [ Preview: https://material.angular.io?theme=purple-green ]
  Custom
? Set up global Angular Material typography styles? Yes 
? Set up browser animations for Angular Material? Yes

ng generate @schematics/angular:module --name=core --project=szkolenie --module=app --no-interactive 
ng generate @schematics/angular:module --name=shared --project=szkolenie --module=app --no-interactive 


npm i -s @angular/flex-layout@10.0.0-beta.32  @angular/cdk

# Playlists
ng generate @schematics/angular:component --name=playlists/components/playlists-list
ng generate @schematics/angular:component --name=playlists/components/playlists-details
ng generate @schematics/angular:component --name=playlists/components/playlist-edit-formng
ng generate @schematics/angular:pipe --name=shared/yesno --project=szkolenie --export --no-flat --no-interactive
ng generate @schematics/angular:service --name=core/services/playlists --no-flat --no-interactive

# UI Library
## generate ui toolkit
ng generate @nrwl/angular:library --name=ui-toolkit --style=scss --addModuleSpec --buildable --importPath=@sgs/ui-toolkit --publishable --strict --no-interactive

## generate widget module
ng generate @schematics/angular:module --name=sample-widget --project=ui-toolkit --module=ui-toolkit --no-interactive 

## generate widget component
ng generate @schematics/angular:component --name=sample-widget --project=ui-toolkit --module=sample-widget --style=scss --changeDetection=OnPush --displayBlock --export --no-interactive 

## Build ui-toolkit
ng build ui-toolkit 


## Link Src vs link dist
<!-- tsconfig.base.json: -->
```json
"paths": {
  // Reload on changes:
  // "@sgs/ui-toolkit": ["libs/ui-toolkit/src/index.ts"]
  // Use Dist - Needs Rebuild
  "@sgs/ui-toolkit": ["dist/libs/ui-toolkit"]
}
```

# Storybook
npm i @nrwl/storybook --save-dev
nx generate @nrwl/angular:storybook-configuration --name=ui-toolkit --configureCypress --generateCypressSpecs --generateStories

nx generate @nrwl/angular:stories --name=ui-toolkit

https://www.learnstorybook.com/intro-to-storybook/angular/en/get-started/

# Knobs
```ts
export const primary = () => ({
  moduleMetadata: {
    imports: []
  },
  component: SampleWidgetComponent,
  props: {
    text: text('text', 'Sample text', 'groupA'),
    active: boolean('active', false, 'groupB'),
    counter: number('counter', 0, { min: 0, max: 100, step: 5 }, 'groupB'),
  }
})
```

# Storybook extras
https://www.npmjs.com/package/@storybook/addon-essentials
https://www.chromatic.com/

# Tabs Components
- UiTabsModule
ng generate @schematics/angular:module --name=ui-tabs --project=ui-toolkit

- UiTabsComponent
ng generate @schematics/angular:component --name=ui-tabs/ui-tabs --project=ui-toolkit --style=scss 
--displayBlock --export

- UiTabComponent
- TabsStories

# Music Search
ng generate @schematics/angular:module --name=music-search --project=szkolenie --module=app --route=music-search --routing --no-interactive

ng generate @schematics/angular:component  --project=szkolenie --no-interactive --name=music-search/components/search-results
ng generate @schematics/angular:component  --project=szkolenie --no-interactive --name=music-search/components/search-form
ng generate @schematics/angular:component  --project=szkolenie --no-interactive --name=music-search/components/album-card
ng generate @schematics/angular:component  --project=szkolenie --no-interactive --name=music-search/containers/album-details


# TypeScript types generation
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype
https://github.com/quicktype/quicktype

# OAuth2.0 Implicit flow
- https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow
- https://manfredsteyer.github.io/angular-oauth2-oidc/
- https://manfredsteyer.github.io/angular-oauth2-oidc/docs/
- https://dev.to/deekshithrajbasa/implicit-flow-authentication-usingangular-oauth2-oidc-angular-2n90
- https://github.com/jeroenheijmans/sample-angular-oauth2-oidc-with-auth-guards
- https://github.com/jeroenheijmans/sample-auth0-angular-oauth2-oidc

# NgRX
npm install --save  @ngrx/schematics @ngrx/store @ngrx/effects @ngrx/entity @ngrx/store-devtools 

ng add @ngrx/store@latest --project=szkolenie

ng generate @ngrx/schematics:store State --root --module app.module.ts
ng generate @ngrx/schematics:effect App --root --module app.module.ts

# Actions
ng generate @ngrx/schematics:action --name=playlist --project=szkolenie --api --group --no-interactive

# Reducer (root)
ng generate @ngrx/schematics:reducer --name=playlists --project=szkolenie --module=app --api --no-flat --group --no-interactive 

# Container (store connected componnet)
 ng generate @ngrx/schematics:container --name=playlists/playlists-ngrx --project=szkolenie --style=scss --changeDetection=OnPush --no-interactive 

 # Selector
 ng generate @ngrx/schematics:selector --name=playlists --group --no-interactive

 # Devtools

 # Effects
 ng generate @ngrx/schematics:effect --name=playlist --project=szkolenie --api --group --root --no-interactive
? To which module (path) should the effect be registered in? app
? Do you want to use the create function? Yes