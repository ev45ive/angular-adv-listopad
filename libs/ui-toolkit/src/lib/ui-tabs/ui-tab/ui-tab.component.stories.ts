
import { UiTabComponent } from './ui-tab.component';

export default {
  title: 'UiTabComponent'
}

export const primary = () => ({
  moduleMetadata: {
    imports: []
  },
  component: UiTabComponent,
  props: {
  }
})