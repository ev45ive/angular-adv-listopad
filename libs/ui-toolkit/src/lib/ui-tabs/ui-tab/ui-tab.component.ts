import { Component, Input, OnInit, Optional, SkipSelf } from '@angular/core';
import { UiTabsComponent } from '../ui-tabs/ui-tabs.component';

@Component({
  selector: 'sgs-ui-tab',
  templateUrl: './ui-tab.component.html',
  styleUrls: ['./ui-tab.component.scss']
})
export class UiTabComponent implements OnInit {

  @Input() title = ''

  @Input() open = false

  toggle() {
    if (this.parent) {
      this.parent.toggle(this)
    } else {
      this.open = !this.open
    }
  }

  // constructor(@Optional() @SkipSelf() private parent: UiTabComponent) {
  constructor(@Optional() private parent: UiTabsComponent) {
    // console.log(parent)
  }
  
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    if(this.parent){
      this.parent.tabs.splice(this.parent.tabs.indexOf(this))
    }
  }

  ngOnInit(): void {
    if(this.parent){
      this.parent.tabs.push(this)
    }
  }

}
