
import { UiTabComponent } from '../ui-tab/ui-tab.component';
import { UiTabsComponent } from './ui-tabs.component';
import { UiTabsModule } from '../ui-tabs.module';
import { number,array } from '@storybook/addon-knobs';
export default {
  title: 'UiTabsComponent'
}

export const SingleTab = () => ({
  moduleMetadata: {
    // declarations: [UiTabsComponent, UiTabComponent],
    imports: [UiTabsModule]
  },
  template: `
    <h3>Test tabs</h3>


      <sgs-ui-tab title="Title B" [open]="true">
        Content B
      </sgs-ui-tab>

      <sgs-ui-tab title="Title C">
        Content C
      </sgs-ui-tab>

  `,
  component: UiTabsComponent,
  props: {
  }
})
export const Accordion = () => ({
  moduleMetadata: {
    // declarations: [UiTabsComponent, UiTabComponent],
    imports: [UiTabsModule]
  },
  template: `
    <h3>Test tabs</h3>

    
    <sgs-ui-tabs>

      <sgs-ui-tab title="Title B">
        Content B
      </sgs-ui-tab>

      <sgs-ui-tab title="Title C">
        Content C
      </sgs-ui-tab>

    </sgs-ui-tabs>
  `,
  component: UiTabsComponent,
  props: {
  }
})

export const Nested = () => ({

  moduleMetadata: {
    // declarations: [UiTabsComponent, UiTabComponent],
    imports: [UiTabsModule]
  },
  template: `
    <h3>Test tabs</h3>

    
    <sgs-ui-tab title="Title A">
      Content A
      <sgs-ui-tab title="Title A">
        Content A
        <sgs-ui-tab title="Title A">
          Content A
          <sgs-ui-tab title="Title A">
            Content A
          </sgs-ui-tab>
        </sgs-ui-tab>
      </sgs-ui-tab>
    </sgs-ui-tab>
  `,
  component: UiTabsComponent,
  props: {
  }
})

export const DynamicAccordion = () => ({
  moduleMetadata: {
    // declarations: [UiTabsComponent, UiTabComponent],
    imports: [UiTabsModule]
  },
  template: `
    <h3>Test tabs</h3>
   
    <sgs-ui-tabs>

      <sgs-ui-tab [title]="'Title '+tab " *ngFor="let tab of tabs">
        Content {{tab}}
      </sgs-ui-tab>

    </sgs-ui-tabs>
  `,
  component: UiTabsComponent,
  props: {
    // count: number('Number of tabs', 2)
    tabs: array('Tabs',[
      'Tab1',
      'Tab2'
    ],'\n')
  }
})
