import { Component, OnInit } from '@angular/core';
import { UiTabComponent } from '../ui-tab/ui-tab.component';

@Component({
  selector: 'sgs-ui-tabs',
  templateUrl: './ui-tabs.component.html',
  styleUrls: ['./ui-tabs.component.scss']
})
export class UiTabsComponent implements OnInit {
  tabs: UiTabComponent[] = []

  toggle(active: UiTabComponent) {
    this.tabs.forEach((tab: UiTabComponent) => {
      tab.open = active === tab
    })
  }

  constructor() { }

  ngOnInit(): void {
  }

}
