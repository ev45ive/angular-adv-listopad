import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiTabsComponent } from './ui-tabs/ui-tabs.component';
import { UiTabComponent } from './ui-tab/ui-tab.component';


import { MatCardModule } from '@angular/material/card'
import { MatIconModule } from '@angular/material/icon'

@NgModule({
  declarations: [UiTabsComponent, UiTabComponent],
  imports: [CommonModule,MatCardModule, MatIconModule],
  exports: [UiTabsComponent,UiTabComponent]
})
export class UiTabsModule { }
