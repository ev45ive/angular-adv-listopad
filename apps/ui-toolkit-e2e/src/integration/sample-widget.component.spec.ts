describe('ui-toolkit', () => {
  beforeEach(() => cy.visit('/iframe.html?id=samplewidgetcomponent--primary'));

  it('should render the component', () => {
    cy.get('sgs-sample-widget').should('exist');
  });
});
