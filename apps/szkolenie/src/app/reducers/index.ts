import { PlaylistsState, playlistsFeatureKey } from './playlists/playlists.reducer';



export type AppState = {
  [playlistsFeatureKey]: PlaylistsState;
};