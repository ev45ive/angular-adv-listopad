import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { ChangeDetectionStrategy, Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { Playlist } from '../core/model/Playlist';
import { PlaylistsService } from '../core/services/playlists/playlists.service'
@Component({
  selector: 'sgs-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss'],
  // encapsulation: ViewEncapsulation.ShadowDom
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsComponent implements OnInit {
  mode: 'details' | 'form' = 'details'

  playlists: Playlist[] = []

  selectedId?: Playlist['id']
  selected?: Playlist

  constructor(private service: PlaylistsService) {
    this.playlists = this.service.getPlaylists()
    this.service.playlistsChange.subscribe((playlists: Playlist[]) => {
      this.playlists = playlists
    })
  }

  selectById(selectedId: Playlist['id']) {
    this.selectedId = this.selectedId == selectedId ? undefined : selectedId;
    if (this.selectedId) {
      this.selected = this.service.getPlaylistById(this.selectedId)
    }
  }

  switchToEdit() {
    this.mode = 'form'
  }

  switchToDetails() {
    this.mode = 'details'
  }

  savePlaylist(draft: Playlist) {
    this.service.savePlaylist(draft)
    // this.playlists = this.service.getPlaylists()
    // this.selected = this.service.getPlaylistById(draft.id)
    this.mode = 'details'
  }


  ngOnInit(): void {
  }

}
