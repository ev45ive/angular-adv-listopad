import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'sgs-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsListComponent implements OnInit {

  // @Input() playlists!: Playlist[] 
  @Input() playlists: Playlist[] = []
  @Input() selectedId?: Playlist['id']

  @Output()
  selectedIdChange = new EventEmitter<Playlist['id']>()

  select(playlist: Playlist) {
    // this.selectedId = playlist.id
    this.selectedIdChange.emit(playlist.id)
  }

  trackFn(index: number, item: Playlist) {
    // console.log(item,index)
    return item.id
  }

  constructor() { 
  }

  ngOnInit(): void {
  }

}
