import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Playlist } from '../../../core/model/Playlist';

@Component({
  selector: 'sgs-playlist-edit-form',
  templateUrl: './playlist-edit-form.component.html',
  styleUrls: ['./playlist-edit-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistEditFormComponent implements OnInit {

  @Input() playlist!: Playlist
  // draft: Playlist

  @Output() onCancel = new EventEmitter();
  @Output() onSave = new EventEmitter<Playlist>();

  save(formRef: NgForm) {
    const data: PlaylistEditFormData = formRef.value
    const draft: Playlist = {
      ...this.playlist,
      ...data
    }

    this.onSave.emit(draft)
  }

  constructor() {
    console.log('constructor')
  }

  ngOnInit(): void {
    console.log('ngOnInit')
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes)
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // this.draft = {...this.playlist}    
  }

  ngDoCheck(): void {
    console.log('ngDoCheck')
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
  }

  @ViewChild('nameRef', { read: ElementRef, static: true })
  nameRef?: ElementRef<HTMLInputElement>

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.nameRef?.nativeElement.focus()
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy')
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
  }
}

interface PlaylistEditFormData {
  name: string
  public: boolean
  description: string
}