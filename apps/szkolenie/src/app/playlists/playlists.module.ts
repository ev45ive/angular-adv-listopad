import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './playlists.component';
import { SharedModule } from '../shared/shared.module';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistsDetailsComponent } from './components/playlists-details/playlists-details.component';
import { PlaylistEditFormComponent } from './components/playlist-edit-form/playlist-edit-form.component';
import { PlaylistsNgrxComponent } from './playlists-ngrx/playlists-ngrx.component';

const routes: Routes = [
  // { path: '', component: PlaylistsComponent }
  { path: '', redirectTo:'test', pathMatch:'full'},
  { path: 'ngrx', component: PlaylistsNgrxComponent }
];

@NgModule({
  declarations: [PlaylistsComponent, PlaylistsListComponent, PlaylistsDetailsComponent, PlaylistEditFormComponent, PlaylistsNgrxComponent],
  imports: [
    // CoreModule,
    CommonModule,
    SharedModule,
    PlaylistsRoutingModule,
    RouterModule.forChild(routes),
    // StoreModule.forFeature(fromPlaylists.playlistsFeatureKey, fromPlaylists.reducer)
  ]
})
export class PlaylistsModule { }
