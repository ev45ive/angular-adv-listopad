import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SomeWidgetComponent } from './some-widget/some-widget.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule } from '@angular/material/list'
import { MatDividerModule } from '@angular/material/divider'
import { MatIconModule } from '@angular/material/icon'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatToolbarModule } from '@angular/material/toolbar';
import { YesnoPipe } from './yesno/yesno.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { SampleWidgetModule } from '@sgs/ui-toolkit';
import { DummyComponent } from './dummy/dummy.component';
import { RecentSearchComponent } from './recent-search/recent-search.component'

@NgModule({
  declarations: [SomeWidgetComponent, YesnoPipe, DummyComponent, RecentSearchComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule,
    MatListModule,
    MatDividerModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,SampleWidgetModule, MatCardModule
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    SomeWidgetComponent,
    MatSidenavModule,
    MatListModule,
    MatDividerModule,
    MatIconModule,
    MatToolbarModule,
    FlexLayoutModule,
    YesnoPipe,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,SampleWidgetModule, MatCardModule, DummyComponent, RecentSearchComponent
  ]
})
export class SharedModule { }
