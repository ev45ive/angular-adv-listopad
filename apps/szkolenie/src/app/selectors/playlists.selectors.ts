import { createFeatureSelector, createSelector } from '@ngrx/store';

import { AppState } from '../reducers'
import { playlistsFeatureKey, PlaylistsState } from '../reducers/playlists/playlists.reducer';

export const selectPlaylistFeature = createFeatureSelector<PlaylistsState>(
  playlistsFeatureKey
)

export const selectPlaylistSelectedId = createSelector(
  selectPlaylistFeature, (state) => state.selectedId
)
export const selectPlaylistList = createSelector(
  selectPlaylistFeature, (state) => state.items
)
export const selectPlaylistMode = createSelector(
  selectPlaylistFeature, (state) => state.mode
)

export const selectPlaylistSelected = createSelector(
  selectPlaylistList,
  selectPlaylistSelectedId,
  (items, id) => items.find(p => p.id === id)
)