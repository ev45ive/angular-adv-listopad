import { createAction, props } from '@ngrx/store';
import { Playlist } from '../core/model/Playlist';

export const loadPlaylists = createAction(
  '[Playlist] Load Playlists'
);

export const loadPlaylistsSuccess = createAction(
  '[Playlist] Load Playlists Success',
  props<{ data: Playlist[] }>()
);

export const loadPlaylistsFailure = createAction(
  '[Playlist] Load Playlists Failure',
  props<{ error: any }>()
);

export const selectPlaylist = createAction(
  '[Playlist] selectPlaylist',
  props<{ id: Playlist['id'] }>()
);

export const editPlaylistMode = createAction(
  '[Playlist] editPlaylistMode',
  props<{ id: Playlist['id'] }>()
);

export const showPlaylistMode = createAction(
  '[Playlist] showPlaylistMode',
  props<{ id: Playlist['id'] }>()
);

export const updatePlaylist = createAction(
  '[Playlist] updatePlaylist',
  props<{ draft: Playlist }>()
);

interface CreatePlaylistDetailsPayload {
  name: Playlist['name']
  public: Playlist['public']
  description: Playlist['description']
}

interface UpdatePlaylistDetailsPayload extends CreatePlaylistDetailsPayload {
  id: Playlist['id']
}

export const savePlaylist = createAction(
  '[Playlist] savePlaylist',
  props<{ draft: UpdatePlaylistDetailsPayload }>()
);

export const savePlaylistSuccess = createAction(
  '[Playlist] savePlaylist Success',
  props<{ data: Playlist }>()
);

export const savePlaylistFailure = createAction(
  '[Playlist] savePlaylist Failure',
  props<{ error: any }>()
);
