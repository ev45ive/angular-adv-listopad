import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchComponent } from './music-search.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: '', component: MusicSearchComponent }
];

@NgModule({
  declarations: [MusicSearchComponent, SearchResultsComponent, SearchFormComponent, AlbumCardComponent, AlbumDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule,
    RouterModule.forChild(routes)
  ]
})
export class MusicSearchModule { }
