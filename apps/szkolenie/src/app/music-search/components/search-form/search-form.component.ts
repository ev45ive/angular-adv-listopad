import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { from, fromEvent, merge, pipe } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeAll, throttleTime } from 'rxjs/operators';

@Component({
  selector: 'sgs-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() query = ''
  // @Input() set query(q:string){ this.searchForm.setValue(...)}

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.searchForm.patchValue({ 
      query: changes['query'].currentValue
    },{
      emitEvent: false/* , onlySelf:true */
    })
  }

  searchForm = new FormGroup({
    'query': new FormControl('batman'),
    'type': new FormControl('album'),
  })

  @Output() search = new EventEmitter<{ query: string }>();
  @Output() searchClick = new EventEmitter();

  submitSearch() {
    this.searchClick.next()
  }

  constructor() { }

  ngOnInit(): void {
    const filterAndDistinct = (length = 3) => pipe(
      // length >= 3
      filter((q: string) => q.length >= length),
      // no duplicates
      distinctUntilChanged(/* comparator */),
    )

    const clicks = this.searchClick.pipe(
      map<any, string>(() => this.searchForm.get('query')?.value),
      throttleTime(1000),
    )

    const typings = this.searchForm.get('query')!.valueChanges.pipe<string>(
      debounceTime(400),
    )

    from([clicks, typings])
      .pipe(
        mergeAll(),
        filterAndDistinct(3),
        map(query => ({ query }))
      )
      .subscribe(this.search)
  }

}
