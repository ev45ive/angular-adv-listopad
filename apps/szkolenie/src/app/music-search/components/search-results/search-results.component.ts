import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Album } from '../../../core/model/Album';

@Component({
  selector: 'sgs-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsComponent implements OnInit {

  @Input() results: Album[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
