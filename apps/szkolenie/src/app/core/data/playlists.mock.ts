import { Playlist } from '../model/Playlist';

export const playlistsMockData: Playlist[] = [{
  id: '123',
  name: 'Mock Playlist 123',
  public: false,
  description: ''
}, {
  id: '234',
  name: 'Mock Playlist 234',
  public: true,
  description: ''
}, {
  id: '345',
  name: 'Mock Playlist 345',
  public: false,
  description: ''
}] as Playlist[]