import { Entity } from "./Entity";


export interface Artist extends Entity {
  type: 'artist';
}
