import { Entity } from './Entity';


export interface Show {
  id: string | number;
  name: string;
  type: 'show'
  public: boolean;
  description: string;
  episodes?: Entity[]
}

// const show: Show = {} as Show;

// if('string' === typeof show.id){
//   show.id.substring(1)
// }else{
//   show.id.toExponential()
// }

// if(show.episodes){ show.episodes.length }
// show.episodes? show.episodes.length : 0
// show.episodes && show.episodes.length 
// show.episodes?.length 

// const result: Show | Playlist = {} as Show | Playlist

// result.type
// if (result.type === 'playlist') {
//   result.tracks
// }else{
//   result.episodes
// }

// switch (result.type) {
//   case 'playlist': result.tracks
//     break;
//   default:
//     // case 'show': 
//     result.episodes
// }

// // DuckTyping / Structural Typing
// interface Vector { x: number; y: number, length: number }
// interface Point { x: number; y: number }

// let v: Vector = { x: 123, y: 234, length: 123 }
// let p: Point = { x: 123, y: 234 }

// // v = p // Not enough coverage
// p = v

