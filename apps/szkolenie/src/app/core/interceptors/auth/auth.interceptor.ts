import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient,
  HttpBackend,
  HttpErrorResponse
} from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';
import { query } from '@angular/animations';
import { catchError, switchMap } from 'rxjs/operators';
import { Album } from '../../model/Album';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private oauthService: OAuthService) { }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler): Observable<HttpEvent<unknown>> {

    const authRequest = this.addAuthorizationHeaders(request)

    return next.handle(authRequest).pipe(
      catchError((error: any, caught) => {
        if (!(error instanceof HttpErrorResponse)) {
          console.error(error)
          return throwError(new Error('Unexpected error'))
        }

        if (error.status === 401) {
          return from(this.oauthService.initLoginFlowInPopup())
            .pipe(switchMap(() => {
              if (!this.oauthService.getAccessToken()) {
                return throwError(new Error())
              }
              const authRequest = this.addAuthorizationHeaders(request)
              return next.handle(authRequest)
            }))
        }
        return throwError(new Error(error.error.error?.message))
      })
    )
  }

  private addAuthorizationHeaders(request: HttpRequest<unknown>) {
    return request/* .clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.oauthService.getAccessToken()
      }
    }); */
  }
}

// HttpClient.handle = A
// A.handle = B 
// B.handle = C
// C.handle = HttpBackend