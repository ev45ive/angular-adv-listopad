import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { playlistsMockData } from './data/playlists.mock'
import { PLAYLISTS_MOCK } from './tokens'
// import { PlaylistsService } from './services/playlists/playlists.service'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OAuthModule, OAuthService, AUTH_CONFIG, AuthConfig } from 'angular-oauth2-oidc';
import { environment } from '../../environments/environment';
import { AuthInterceptor } from './interceptors/auth/auth.interceptor'
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['https://api.spotify.com/v1'],
        sendAccessToken: true
      }
    })
  ],
  providers: [
    // {
    //   provide: HttpClient,
    //   useClass: MyBetterAUthHttpClient
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: AuthInterceptor,
      multi: true
    },
    AuthInterceptor,
    {
      // provide: 'PLAYLISTS_MOCK',
      provide: PLAYLISTS_MOCK,
      useValue: playlistsMockData
    },
    // {
    //   provide: PlaylistsService,
    //   useFactory(data: Playlist[]/* valueB, instanceC */) {
    //     return new PlaylistsService(data)
    //   },
    //   deps: [PLAYLISTS_MOCK/* , TOKENB, TOKENC */]
    // },
    // {
    //   provide: PlaylistsService,
    //   useClass: PlaylistsService,
    //   // deps: [PLAYLISTS_MOCK/* , TOKENB, TOKENC */] // > from @Inject
    // },
    // {
    //   provide: AbstractPlaylistsService,
    //   useClass: SpotifyPlaylistsService
    // },
    // PlaylistsService,
    // {
    //   provide: RoseUnderADifferentName,
    //   useExisting: RoseService
    // }
    {
      provide: AuthConfig,
      useValue: environment.authCodeFlowConfig
    }
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() self: CoreModule,
    private oauthService: OAuthService) {
    if (self) {
      throw new Error('Core module should be imported only once in root module')
    }

    // this.oauthService.configure(environment.authCodeFlowConfig);
    this.oauthService.setupAutomaticSilentRefresh();

    this.oauthService.tryLogin({}).then(() => {
      if (!this.oauthService.hasValidAccessToken()) {
        // this.oauthService.initImplicitFlow(); // redirect
        // this.oauthService.initLoginFlowInPopup() // popup
      }

      const token = this.oauthService.getAccessToken()
      console.log(token)
    })
  }
}
