import { HttpClient } from '@angular/common/http';
import { EventEmitter, Inject, Injectable, Optional } from '@angular/core';
import { map } from 'rxjs/operators';
import { PagingObject } from '../../model/PagingObject';
import { Playlist } from '../../model/Playlist';
import { PLAYLISTS_MOCK } from '../../tokens'

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {

  playlistsChange = new EventEmitter<Playlist[]>()

  constructor(
    private http:HttpClient,
    @Optional() @Inject(PLAYLISTS_MOCK)
    private playlists: Playlist[] = []) {
  }

  fetchPlaylists(){
    return this.http.get<PagingObject<Playlist>>(`https://api.spotify.com/v1/me/playlists`).pipe(map(res => res.items))
  }

  getPlaylists() {
    return this.playlists
  }

  getPlaylistById(id: Playlist['id']): Playlist | undefined {
    return this.playlists.find(p => p.id === id)
  }

  savePlaylist(draft: Playlist) {
    // Mutable - Wont triffer changes with OnPush
    // const index = this.playlists.findIndex(p => p.id == draft.id)
    // if(index !== -1){
    //   this.playlists.splice(index,1,draft)
    // }

    // Immutable - change requires new object reference / copy
    this.playlists = this.playlists.map(p => p.id === draft.id ? draft : p)
    this.playlistsChange.emit(this.playlists)
  }

}
