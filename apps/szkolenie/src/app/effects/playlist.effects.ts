import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { EMPTY, from, of } from 'rxjs';
import { mergeMap, map, catchError, switchMap } from 'rxjs/operators';
import * as playlistActions from '../actions/playlist.actions';
import { PlaylistsService } from '../core/services/playlists/playlists.service';



@Injectable()
export class PlaylistEffects {

  constructor(private actions$: Actions, private service: PlaylistsService) { }


  loadPlaylists$ = createEffect(() => this.actions$.pipe(
    ofType(playlistActions.loadPlaylists),
    switchMap((action) => this.service.fetchPlaylists().pipe(
      map(data => playlistActions.loadPlaylistsSuccess({ data })),
      catchError(error => of(playlistActions.savePlaylistFailure({ error })))
    ))
  ))

  loadPlaylistsAndSmth$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    mergeMap((action) => {
      return from([
        playlistActions.selectPlaylist({ id: '123' }),
        playlistActions.selectPlaylist({ id: '234' }),
        playlistActions.loadPlaylists(),
      ])
    })

  ))
}
